using aspnetTestApi.Models.AppsettingsModels;
using aspnetTestApi.Services;

var builder = WebApplication.CreateBuilder(args);

#region DependancyInjection

// Configs
builder.Services.Configure<AppsettingsTestModel>(builder.Configuration.GetSection("Test"));

// Services
builder.Services.AddScoped<IWeatherForecastService,  WeatherForecastService>();

#endregion

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
