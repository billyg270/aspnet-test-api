﻿using aspnetTestApi.Models;

namespace aspnetTestApi.Services
{
    public interface IWeatherForecastService
    {
        public WeatherForecastModel[] GetWeatherForecast();
    }
}
