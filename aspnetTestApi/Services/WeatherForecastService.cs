﻿using aspnetTestApi.Models;
using aspnetTestApi.Models.AppsettingsModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace aspnetTestApi.Services
{
    public class WeatherForecastService : IWeatherForecastService
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        AppsettingsTestModel _appsettingsTest;

        public WeatherForecastService(
            IOptions<AppsettingsTestModel> appsettingsTest
        ) {
            _appsettingsTest = Util.AppsettingsUtil.ApplyStringEnvValuesOnIOptionsRecurcive(appsettingsTest);
        }

        public WeatherForecastModel[] GetWeatherForecast()
        {
            return Enumerable.Range(1, 5).Select(index => new WeatherForecastModel
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = Random.Shared.Next(-20, 55),
                Summary = Summaries[Random.Shared.Next(Summaries.Length)] + " " + _appsettingsTest.Message
            })
            .ToArray();
        }
    }
}
