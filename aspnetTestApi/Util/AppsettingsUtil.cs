﻿using aspnetTestApi.Models.AppsettingsModels;
using Microsoft.Extensions.Options;
using System.Reflection;
using System.Text.RegularExpressions;

namespace aspnetTestApi.Util
{
    public static class AppsettingsUtil
    {
        public static string ApplyStringEnvValuesOnString(string input)//Example input: "Hello {{NAME}}, today is {{DAY}}"
        {
            Regex regex = new(@"\{\{(.*?)\}\}");

            return regex.Replace(input, m =>
            {
                string variableName = m.Groups[1].Value;
                string? variableValue = Environment.GetEnvironmentVariable(variableName);
                return variableValue ?? throw new Exception("Enviroment variable not set that appsettings references: " + variableName);
            });
        }

        public static object ApplyStringEnvValuesOnObjectRecurcive(object obj, string basePath)
        {
            Type type = obj.GetType();

            static object DoRecursive(Type type, object obj2, string path)
            {
                PropertyInfo[] pi = type.GetProperties();
                foreach (PropertyInfo p in pi)
                {
                    if (
                        p.PropertyType.IsClass &&
                        !p.PropertyType.IsValueType &&
                        !p.PropertyType.IsPrimitive &&
                        p.PropertyType.FullName != "System.String"
                    )
                    {
                        object? subObject = p.GetValue(obj2);
                        if (subObject != null)
                        {
                            p.SetValue(
                                obj2,
                                DoRecursive(p.PropertyType, subObject, path + "." + p.Name)
                            );
                        }
                    }

                    if(p.PropertyType == typeof(string))
                    {
                        Console.WriteLine("APPSETTINGS_" + path + "." + p.Name);
                        string? envVar = Environment.GetEnvironmentVariable("APPSETTINGS_" + path + "." + p.Name);

                        if (envVar != null)
                        {
                            p.SetValue(
                                obj2,
                                envVar
                            );
                        }
                        else
                        {
                            object? subObject = p.GetValue(obj2);
                            if (subObject != null)
                            {
                                p.SetValue(
                                    obj2,
                                    ApplyStringEnvValuesOnString(
                                        (string)subObject
                                    )
                                );
                            }
                        }
                    }
                }

                return obj2;
            }

            return DoRecursive(type, obj, basePath);
        }

        public static T ApplyStringEnvValuesOnIOptionsRecurcive<T>(IOptions<T> options) where T : class
        {
            string name = options.Value.GetType().Name;
            name = name.Substring(11, name.Length - 11 - 5);
            return (T)ApplyStringEnvValuesOnObjectRecurcive(options.Value, name);
        }
    }
}
